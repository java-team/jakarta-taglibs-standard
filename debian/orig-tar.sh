#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=jakarta-taglibs-standard-$2
TAR=../jakarta-taglibs-standard_$2.orig.tar.gz

# clean up the upstream tarball
tar zxvf $3
mv $DIR-src $DIR
tar -c -z -f $TAR $DIR
rm -rf $DIR

exit 0
